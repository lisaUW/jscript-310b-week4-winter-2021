/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = () => {

}



// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);

function createCard(val,displayVal,suit) {
  return {
    val,
    displayVal,
    suit,
  };
}

/////////
const getDeck = () => {
  let completeDeck = []
  // create card objects
  const suits = ['hearts', 'spades', 'clubs', 'diamonds']
  suits.forEach((suit) => {
    
    // count to 13
    const numCards = 13
    // check the index of that count
    for (let index = 1; index <= numCards; index++) {
      if (index === 1) {
        completeDeck.push(createCard(11,'Ace',suit))
      }
      else if (index > 1 && index < 11) {
        completeDeck.push(createCard(index,index.toString(),suit))
      }
      else if (index === 11) {
        completeDeck.push(createCard(10,'Jack',suit)); 
      }
      else if (index === 12) {
        completeDeck.push(createCard(10,'Queen',suit)); 
      }
      else if (index === 13) {
        completeDeck.push(createCard(10,'King',suit)); 
      }
      // else console.log(index);
    }
  })
  return completeDeck
}