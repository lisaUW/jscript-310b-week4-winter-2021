const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {}; //TODO'

class cardPlayer {
  constructor(name) {
    this.name = name;
  }
}



// CREATE TWO NEW CardPlayers
const dealer; // TODO
const player; // TODO

new cardPlayer("dealer");
new cardPlayer("Elsa")
/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
    total = 0;
    hand.forEach((card) => {
        total += card.val;
      }
      return total;
      // CREATE FUNCTION HERE
      //iterating over hand array with forEach method passing card element and adding up val of each card 
    }




    /**
     * Determines whether the dealer should draw another card.
     * 
     * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
     * @returns {boolean} whether dealer should draw another card
     */
    const dealerShouldDraw = (dealerHand) => {
        // CREATE FUNCTION HERE
        if (calcPoints(dealerHand) <= 16) {
          return true;
        } else {
          return false;
        }
        };
        /**
         * Determines the winner if both player and dealer stand
         * @param {number} playerScore 
         * @param {number} dealerScore 
         * @returns {string} Shows the player's score, the dealer's score, and who wins
         */
        const determineWinner = (playerScore, dealerScore) => {
          // CREATE FUNCTION HERE
            console.log(`Player Score is ${playerScore} vs. Dealer Score is ${dealerScore}.`)
            if (playerScore < dealersScore){
              console.log("Dealer has won!")
            } else {
              console.log("Player has won!")
            }
            
        }

        /**
         * Creates user prompt to ask if they'd like to draw a card
         * @param {number} count 
         * @param {string} dealerCard 
         */
        const getMessage = (count, dealerCard) => {
          return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
        }

        /**
         * Logs the player's hand to the console
         * @param {CardPlayer} player 
         */
        const showHand = (player) => {
          const displayHand = player.hand.map((card) => card.displayVal);
          console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
        }

        /**
         * Runs Blackjack Game
         */
        const startGame = function () {
          player.drawCard();
          dealer.drawCard();
          player.drawCard();
          dealer.drawCard();

          let playerScore = calcPoints(player.hand).total;
          showHand(player);
          while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
            player.drawCard();
            playerScore = calcPoints(player.hand).total;
            showHand(player);
          }
          if (playerScore > 21) {
            return 'You went over 21 - you lose!';
          }
          console.log(`Player stands at ${playerScore}`);

          let dealerScore = calcPoints(dealer.hand).total;
          while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
            dealer.drawCard();
            dealerScore = calcPoints(dealer.hand).total;
            showHand(dealer);
          }
          if (dealerScore > 21) {
            return 'Dealer went over 21 - you win!';
          }
          console.log(`Dealer stands at ${dealerScore}`);

          return determineWinner(playerScore, dealerScore);
        }
        // console.log(startGame());